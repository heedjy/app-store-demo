# Example marketplace

# Getting started
You first need an Heedjy client

See Run section for environment variables
```
cp .env.local.example .env.local
```

install node dependencies
```
yarn install
```

start the local dev server (localhost:3000) with hot-reload 

```
yarn dev
```

run unit tests
```
yarn test
```

## Environment variables

| Environment variable | Description                                            |
|----------------------|--------------------------------------------------------|
| API_BASE_URI         | base uri of Heedjy API                                 |
| API_CLIENT_ID        | ID of the marketplace site (marketplace/site settings) |
| API_CLIENT_SECRET    | SECRET of the marketplace site                         |

# Production build & run

```
docker build . -t website-demo
```
Run on 3000 port
```
docker run --rm -p 3000:3000 \
  -e API_BASE_URI=https://app.heedjy.com \
  -e API_CLIENT_ID=xxxxx \
  -e API_CLIENT_SECRET=yyyyy \
  website-demo
```
