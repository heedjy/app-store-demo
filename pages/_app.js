import '../global.css'
import {PageContext} from "../components/PageContext";
import API from "./api/API";
import {useRouter} from "next/router";

export default function MyApp({ Component, pageProps, appDefinition }) {
    const router = useRouter();//to force rebuild component when changing route (categories for example)

    // Use the layout defined at the page level, if available
    const getLayout = Component.getLayout || ((page) => page)

    return <PageContext.Provider value={{appDefinition:appDefinition}}>
        {getLayout(
            <Component {...pageProps} key={router.asPath}/>)}
        </PageContext.Provider>
}

MyApp.getInitialProps = async() => {

    // Fetch data from external API
    const appDefinition = await new API().getAppDefinition();

    // Pass data to the page via props
    return {appDefinition}
}