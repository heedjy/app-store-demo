
import Layout from '../components/layout'
import Sidebar from '../components/sidebar'
import API from "./api/API";

export default function Index({data}) {
    return ""
}

Index.getLayout = function getLayout(page) {
    return (
        <Layout>
            <Sidebar />
            {page}
        </Layout>
    )
}

// This gets called on every request
export async function getServerSideProps() {

    return {
        redirect: {
            destination: '/apps',
            permanent: false,
        },
    }
}