import axios from 'axios';
import cacheManager from 'cache-manager';

//a simple in memory cache. Should use a filesystem or Redis in production.
const tokenCache = cacheManager.caching({store: 'memory', max: 100, ttl: 300/*seconds*/});
const queryCache = cacheManager.caching({store: 'memory', max: 100, ttl: 600/*seconds*/});

const API_BASE_URI = process.env.API_BASE_URI;
const API_CLIENT_ID = process.env.API_CLIENT_ID;
const API_CLIENT_SECRET = process.env.API_CLIENT_SECRET;

export default class API {

    async getToken() {
        return tokenCache.wrap("token", async () => {
            const res = await axios.post(`${API_BASE_URI}/api/oauth2/token`,
                `grant_type=client_credentials&client_id=${API_CLIENT_ID}&client_secret=${API_CLIENT_SECRET}`);

            return res.data.access_token;
        });
    }

    getAppDefinition() {

        const key = "app-definitions";

        return queryCache.wrap(key, async () => {

            const token = await this.getToken();

            const res = await axios.get(`${API_BASE_URI}/api/app-definitions`, {
                headers: {'content-type': 'application/json', authorization: `Bearer ${token}`}
            });

            return res.data;
        });

    }

    async getApps(query, cache = true) {

        const fun = async () => {

            const token = await this.getToken();

            const res = await axios.get(`${API_BASE_URI}/api/public/published-apps`, {
                params: query,
                headers: {'content-type': 'application/json', authorization: `Bearer ${token}`}
            });

            return res.data;
        }

        if (cache) {
            const key = "published-apps-" + JSON.stringify(query);
            return queryCache.wrap(key, fun);
        } else {
            return fun();
        }

    }

    async getAppDetail(id) {
        const key = `published-apps/${id}`;

        return queryCache.wrap(key, async () => {
            const token = await this.getToken();

            const res = await axios.get(`${API_BASE_URI}/api/public/published-apps/${id}`, {
                headers: {'content-type': 'application/json', authorization: `Bearer ${token}`}
            });

            res.data.demo_url = "https://www.youtube.com/watch?v=rT1XKdk3bxE";

            return res.data;
        });

    }

}