import API from "./API";

export default async function handler(req, res) {

    const query = req.query

    if(!query.text || query.text.trim().length === 0)
        delete query.text;

    const useCache = !query.text;
    const page = await new API().getApps(query, useCache);

    res.status(200).json(page);
}