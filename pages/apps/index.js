import Link from 'next/link'
import Image from 'next/image'
import {useContext, useState} from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import axios from 'axios';

import API from "../api/API";
import Layout from '../../components/layout'
import {PageContext} from "../../components/PageContext";
import SearchBar from "../../components/SearchBar";

import styles from './index.module.css'

const PAGE_SIZE = 20;

export default function Index({initialPage, initialQuery}) {
    const {appDefinition} = useContext(PageContext);

    const [apps, setApps] = useState(initialPage.items);
    const [page, setPage] = useState(initialPage);

    function loadMore() {
        const cursor = new URL("https://dummy" + page.next).searchParams.get("cursor");
        if (cursor) {
            const query = {...initialQuery, cursor: cursor}

            axios.get(`/api/apps`, {
                params: query
            }).then(newPage => {
                setPage(newPage.data);
                setApps((apps) => [...apps, ...newPage.data.items])
            });
        }
    }

    function onSearch(text) {
        text = text.trim();
        if (text.length !== 1) {

            const query = {...initialQuery, text: text, cursor: null}

            axios.get(`/api/apps`, {
                params: query
            }).then(newPage => {
                setPage(newPage.data);
                setApps(newPage.data.items)
            });
        }
    }

    const custom_property = (app, property_name) => app.custom_properties.find(cp => cp.name === property_name);
    const category_name = (cat_id) => appDefinition[0].categories_definitions.find(cd => cd.id === cat_id).label;

    return (
        <>
            <h2>{initialQuery?.category ? category_name(initialQuery?.category) : "All Apps"}</h2>
            <SearchBar onSearch={onSearch}/>
            <div className={styles.list}>
                <br/>
                <section>
                    <InfiniteScroll
                        className={styles.cards}
                        dataLength={apps.length}
                        next={loadMore}
                        hasMore={page.next !== null}
                        loader={<h3> Loading...</h3>}
                        endMessage={<h4>Nothing more to show</h4>}
                    >
                        {apps.map(app =>
                            <article key={app.id} className={styles.card}>
                                <Link href={"/apps/" + app.slug}>
                                    <a>
                                        <div className={styles.inner}>
                                            <div className={styles.logo}>
                                                <Image src={app.logo} width="100px" height="100px" objectFit="contain"
                                                       alt="app logo"/>
                                            </div>
                                            <div className={styles.cardContent}>
                                                <h2>{app.title}</h2>
                                                <p>
                                                    <span
                                                        className={styles.badge}>{custom_property(app, "extension_type")?.value.toUpperCase()}</span>
                                                    {app.categories.map(cat =>
                                                        <span key={cat.id}
                                                              className={styles.badge}>{cat.label.toUpperCase()}</span>)
                                                    }

                                                </p>
                                                <p dangerouslySetInnerHTML={{__html: custom_property(app, "short_description")?.value}}/>
                                                <p><strong>Price: {custom_property(app, "price_type")?.value}</strong>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </Link>
                            </article>)}
                    </InfiniteScroll>
                </section>
            </div>
        </>
    )
}

Index.getLayout = function getLayout(page) {
    return (
        <Layout sideBar={true}>
            <div className={styles.layout}>
                {page}
            </div>
        </Layout>
    )
}

// This gets called on every request
export async function getServerSideProps({query}) {

    query.limit = PAGE_SIZE;

    // Fetch data from external API
    const page = await new API().getApps(query);

    // Pass data to the page via props
    return {props: {initialPage: page, initialQuery: query}}
}