import Link from 'next/link'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import Layout from "../../components/layout";
import styles from "./[id].module.css";
import API from "../api/API";

export default function AppDetail({data, origin}) {

    /* if it's a youtube link we'll use the iframe player */
    let demo_url = null;
    let isYoutube = false;
    if(data.demo_url) {
        demo_url = new URL(data.demo_url);
        isYoutube = demo_url.origin.includes("youtube");
        if (isYoutube && demo_url.pathname.includes("watch")) {
            demo_url = new URL(demo_url.origin + "/embed/" + demo_url.searchParams.get("v") + "?origin=" + origin)
        }
    }

    data.demo_url = demo_url;
    data.isYoutube = isYoutube;


    const custom_property = (app, property_name) => app.custom_properties.find(cp => cp.name === property_name);

    return (
        <div className={styles.detail}>
            <div className={styles.back}>
                <Link href="/apps">
                    <a><img src="/arrow.svg" alt="back icon"/> Browse apps</a>
                </Link>
            </div>
            <div className={styles.header1}>
                    <h1>{data.title}</h1>
                    <p>
                        <span className={styles.badge}>{custom_property(data,"extension_type").value.toUpperCase()}</span>
                        {data.categories.map(cat =>
                             <span key={cat.id} className={styles.badge}>{cat.label.toUpperCase()}</span>)
                        }
                    </p>
            </div>
            <div className={styles.header2}>
                <div><span>Company:</span> {data.company_name}</div>
                <div><span>Version:</span> {data.version}</div>
            </div>
            <div className={styles.carousel}>
                <Carousel>
                    {custom_property(data, "gallery").value.map((file,i) =>
                        <div key={"gallery"+i}>
                            <img src={file.ref}/>
                            <p className="legend">Legend 1</p>
                        </div>)
                    }
                </Carousel>
            </div>
            <div>
                <h2>Description</h2>
                <div dangerouslySetInnerHTML={{__html:data.description}} />
            </div>
            <div>
                <h2>Demo</h2>
                <div className={styles.demoContainer}>
                    {data.isYoutube &&
                        <iframe id="player" title="youtube video"
                                src={data.demo_url}
                                allowFullScreen="allowfullscreen"/>}
                    {!data.isYoutube && <a href={data.demo_url}>{data.demo_url}</a>}
                </div>
            </div>
        </div>
    )
}


AppDetail.getLayout = function getLayout(page) {
    return (
        <Layout sideBar={false}>
            <div style={{width:"100%"}}>
                {page}
            </div>

        </Layout>
    )
}

// This gets called on every request
export async function getServerSideProps(context) {
    // Fetch data from external API
    const data = await new API().getAppDetail(context.query.id);
    const origin = context.req?.headers['host'] || "";
    // Pass data to the page via props
    return { props: { data, origin } }
}