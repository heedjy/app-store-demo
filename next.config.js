module.exports = {
    i18n: {
        locales: ["en"],
        defaultLocale: "en",
    },
    images: {
        domains: ['localhost', 'cdn.heedjy.com'],
    },
};