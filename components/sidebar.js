import styles from './sidebar.module.css'
import {useContext} from "react";
import {PageContext} from "./PageContext";
import NavLink from "./NavLink";


export default function Sidebar({ active, setActive}) {

    const {appDefinition} = useContext(PageContext);
    return (
        <nav className={styles.nav + (active ? " "+styles.active : "")}>
            <div>
                <h2>Filters</h2>
                <div>
                    <NavLink href="/apps" activeClassName={styles.activeLink} onClick={() => {
                        setActive(false)}}>
                        All apps
                    </NavLink>
                </div>
            </div>
            <div>
                <h2>Categories</h2>
                <div>
                    {appDefinition[0].categories_definitions?.map(cd =>
                    <NavLink href={"/apps?category="+cd.id} key={cd.id} activeClassName={styles.activeLink} onClick={() => {
                        setActive(false)
                    }}>
                        {cd.label}
                    </NavLink>
                    )}
                </div>
            </div>
        </nav>
    )
}
