import {useRouter} from "next/router";
import Link from "next/link";

export default ({ activeClassName, href, children, passHref, onClick }) => {
    const router = useRouter();

    return (
        <Link href={href} passHref={passHref}>
            <a className={router.asPath === href ? activeClassName : ""} onClick={onClick}>
                {children}
            </a>
        </Link>
    );
};