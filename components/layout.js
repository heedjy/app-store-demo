import {useState} from "react"
import Head from 'next/head'
import Image from 'next/image'

import styles from './layout.module.css'
import Sidebar from "./sidebar"
import Logo from "../public/akeneo-logo.png"

export default function Layout({sideBar, children }) {
    const [respActive, setRespActive] = useState(false);

    return (
        <>
            <Head>
                <title>Example Marketplace</title>
                <meta name="description" content="Example marketplace" />
                <link rel="icon" type="image/jpg" href="/favicon.jpg" />
                <link rel='preconnect' href='https://fonts.gstatic.com' crossOrigin="true" />
                <link rel="preload" href="https://fonts.googleapis.com/css?family=Inter&display=block" as="style" />
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter&display=block" />
            </Head>
            <div className={styles.topBar}>
                <div className={styles.navToggle + (respActive ? "": " "+styles.active)} onClick={() => {
                    setRespActive(!respActive)
                }}>
                    <span className={styles.iconBar}/>
                    <span className={styles.iconBar}/>
                    <span className={styles.iconBar}/>
                </div>
                <div className={styles.title}>
                    <div className={styles.logo}>
                        <Image src={Logo} alt="Company logo"/>
                    </div>
                    <h1>Akeneo App Store</h1>
                </div>
            </div>
            <div className={styles.menuOverlay + (respActive ? " "+styles.active : "")} onClick={() => {
                setRespActive(!respActive)
            }}/>
            <main className={styles.main}>
                {sideBar && <Sidebar active={respActive} setActive={setRespActive}/>}
                {children}
            </main>
        </>
    )
}