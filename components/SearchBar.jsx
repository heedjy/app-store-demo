import React, {useCallback, useState} from "react";
import debounce from "lodash.debounce";
import styles from "./SearchBar.module.css";

export default ({defaultValue, onSearch, ...props}) => {

    const DEBOUNCE_WAIT_MS = 200;

    const [text, setText] = useState(defaultValue || "");

    //use callback to make debounce work in react (memoize debounce function so it is not recreated every time component renders)
    const debouncedHandleSuggest = useCallback(debounce(onSearch, DEBOUNCE_WAIT_MS), []);


    function handleChange(e) {
        e.preventDefault();

        const value = e.target.value;

        setText(value);

        return debouncedHandleSuggest(value);
    }


    return <div className={styles.searchBar}>

        <div style={{position: "relative"}}>
            <input id={"id-" + props.name} type="text" placeholder="Search..." className={styles.searchInput}
                   autoComplete="off" aria-autocomplete="both" autoCorrect="off" spellCheck="off" role="combobox"
                   value={text} onChange={handleChange}
                   {...props}
            />
            <svg className={styles.searchIcon} width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect x="2" y="2" width="17" height="17" rx="8.5" stroke="#97A2AA" strokeWidth="1.4"/>
                <line x1="16.5256" y1="17.5355" x2="20.5356" y2="21.5456" stroke="#97A2AA" strokeWidth="1.4"
                      strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
        </div>
    </div>
}
